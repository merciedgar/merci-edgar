# Suppression de Cucumber

## Context

Il y avait un repertoire avec des `features` cucumber. On pensait que le
PO/Intrapreneur/Client jeterais un coup d'oeil aux spec. Finalement, il
ne regarde pas, et on doit maintenir un repertoire de `features` et un
repertoire de `spec`.

_Cette décision est documenté à posteriori, juste pour essayer ce format_

## Decision

Nous avons ramené toutes les `features` dans un repertoire
`spec/features`. Comme ça, tous les tests sont dans le même répertoire,
et on les lancent tous en même temps.

## Status

Fini.


## Conséquences

Les tests sont un peu plus long. Il faut faire une action partiuclière
pour ne pas lancer des tests bout en bout.
