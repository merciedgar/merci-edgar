---
adherent: "Prénom Nom du manager compte (Nom du compte)"
montant: 50€
date_souscription: 25/09/2020
date_expiration: 25/09/2021
---
Merci Edgar  
13 rue Truillot  
Bâtiment C15  
94200 Ivry sur Seine  
N°RNA: W782005393  

## A l'attention de $adherent$

# Objet : Reçu de cotisation

Cher membre adhérent,

Nous avons bien reçu votre déclaration d'adhésion en date du $date_souscription$ ainsi que le règlement de votre cotisation et nous vous en remercions. Pour les besoins de votre comptabilité, nous attestons par la présente que vous avez dûment acquitté auprès de notre association :

- un montant de $montant$
- en date du $date_souscription$ au titre de la cotisation due à raison de votre adhésion à notre association pour la période allant du $date_souscription$ jusqu'au $date_expiration$

Nous vous rappelons que la cotisation n’est pas soumise à la TVA et qu’elle ne donne pas lieu à la délivrance d’une facture. Elle n’ouvre pas droit au bénéfice des dispositions des articles 200, 238 bis et 885-0 V bis Adu code général des impôts.

Nous vous prions d’agréer, cher membre adhérent, nos sincères salutations.

Le président  
Christophe Robillard

![](signature.png)

[//]: # Pour générer un pdf : `pandoc --template recu_cotisation.md recu_cotisation.md | pandoc -o recu.pdf`
