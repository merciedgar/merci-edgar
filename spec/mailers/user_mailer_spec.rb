require "rails_helper"

describe UserMailer do
  describe "welcome email" do
    it "works" do
      user = FactoryGirl.create(:user)
      email = UserMailer.welcome_email(user)
      expect(email.to).to eq([user.email])
      expect(email.body.encoded).to match('Welcome')
      expect(email.subject).to match('Request Received')
    end
  end

  describe "subscription receipt email" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, manager: true, account: account)
      email = UserMailer.subscription_receipt_email(account, manager, 20)
      expect(email.to).to eq([manager.email])
      expect(email.subject).to match('Adhesion validee')
      expect(email.body.encoded). to match('20')
    end
  end

  describe "#export_contacts" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, manager: true, account: account)
      email = UserMailer.export_contacts(manager, "url,zip")
      expect(email.to).to eq([manager.email])
      expect(email.subject).to match('Export de votre compte Merci Edgar ')
      expect(email.body.encoded). to match('export')
    end
  end

  describe "#contacts_import_error" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, manager: true, account: account)
      email = UserMailer.contacts_import_error(manager)
      expect(email.to).to eq([manager.email])
      expect(email.subject).to match("Problème lors de l'import Merci Edgar")
      expect(email.body.encoded). to match('erreur')
    end
  end

  describe "#upgrade_receipt_email" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, manager: true, account: account)
      email = UserMailer.upgrade_receipt_email(account, manager, 100)
      expect(email.to).to eq([manager.email])
      expect(email.subject).to match("Mise à jour de votre compte validee")
      expect(email.body.encoded). to match('complément de 100€')
    end
  end

  describe "#contacts_import_invalid" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, manager: true, account: account)
      email = UserMailer.contacts_import_invalid(manager)
      expect(email.to).to eq([manager.email])
      expect(email.subject).to match("Problème lors de l'import Merci Edgar")
      expect(email.body.encoded). to match('mal construit')
    end
  end

  describe "#contacts_import_email" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, manager: true, account: account)
      options = {filename: "a_filename", account: account, imported_at: DateTime.new(2017,4,23,13,46)}
      email = UserMailer.contacts_import_email(manager, options)
      expect(email.to).to eq([manager.email])
      expect(email.subject).to match("Import de contacts")
      expect(email.body.encoded). to match('a_filename')
    end
  end

  describe "#abilitation_notification" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, manager: true, account: account)
      member = FactoryGirl.create(:user, manager: false, account: account)
      email = UserMailer.abilitation_notification(account, manager, member)
      expect(email.to).to eq([member.email])
      expect(email.subject).to match("Bienvenue dans la base de Test account (Merci Edgar)")
      expect(email.body.encoded). to match('la base Merci Edgar du compte Test account')
    end
  end

  describe "#abilitation_instructions" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, manager: true, account: account)
      member = FactoryGirl.create(:user, manager: false, account: account)
      email = UserMailer.abilitation_instructions(account, manager, member)
      expect(email.to).to eq([member.email])
      expect(email.subject).to match("Bienvenue dans la base de Test account (Merci Edgar)")
      expect(email.body.encoded). to match('la base Merci Edgar du compte Test account')
    end
  end

  describe "subscription official receipt email" do
    let(:account) { FactoryGirl.create(:account) }
    let!(:manager) { FactoryGirl.create(:user, manager: true, account: account) }
    let(:email) { UserMailer.subscription_official_receipt_email(account) }
    it { expect(email.to).to eq([manager.email]) }
    it { expect(email.body.encoded).to match('20') }
    it { expect(email.attachments.count).to eq(1) }
    it { expect(email.subject).to match('Recu adhesion Merci Edgar') }
  end
end
