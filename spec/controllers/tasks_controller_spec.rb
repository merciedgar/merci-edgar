require "rails_helper"

describe TasksController do

  describe "#new" do
    it "works" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :new
      expect(response).to be_success
    end
  end

  describe "#edit" do
    it "works" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      task = FactoryGirl.create(:task, account: account)

      get :edit, id: task.id
      expect(response).to be_success
    end
  end

  describe "#update" do
    it "works" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager
      task = FactoryGirl.create(:task, account: account)

      put :update, id: task.id, format: :js

      expect(response).to be_success
    end

    it "works with asset" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager
      contact = FactoryGirl.create(:contact, account: account)
      task = FactoryGirl.create(:task, account: account, asset: contact)

      put :update, id: task.id, format: :js

      expect(response).to be_success
    end
  end

  describe "#complete" do
    it "works" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager
      task = FactoryGirl.create(:task, account: account)

      put :complete, id: task.id, format: :js

      expect(response).to be_success
    end
  end

  describe "#uncomplete" do
    it "works" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager
      task = FactoryGirl.create(:task, account: account)

      put :uncomplete, id: task.id, format: :js

      expect(response).to be_success
    end
  end

  describe "#create" do
    it "return error" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      post :create, task: {user_id: manager.id}, format: :js

      expect(response).to be_error
    end

    it "redirect when without asset" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      tasks_attributes = {name: "Something", user_id: manager.id}

      post :create, task: tasks_attributes, format: :js

      expect(response).to be_success
    end

    it "works with an asset" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      contact = FactoryGirl.create(:account)

      tasks_attributes = {name: "Something", user_id: manager.id, asset_id: contact.id, asset_type: Contact}

      post :create, task: tasks_attributes, format: :js

      expect(response).to be_success
    end


  end

  describe "#index" do
    it "works" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :index
      expect(response).to be_success
    end

    it "can render ics format" do
      account = FactoryGirl.create(:account)
      address = FactoryGirl.create(:address, account: account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :index, format: :ics
      expect(response).to be_success
    end
 
  end
end
