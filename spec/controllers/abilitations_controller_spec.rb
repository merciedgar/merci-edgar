require "rails_helper"

describe AbilitationsController do
  describe "#new" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :new, format: 'js'

      expect(response).to be_success
    end
  end

  describe "#create" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      post :create, format: 'js', abilitation: {user_attributes: {email: 'henri@ford.com'}}

      expect(response).to be_redirect
    end
  end

  describe "#destroy" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      abilitation = FactoryGirl.create(:abilitation)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      post :destroy, id: abilitation.id, abilitation: {user_attributes: {email: 'other@ford.com'}}

      expect(response).to be_redirect
    end
  end

end
