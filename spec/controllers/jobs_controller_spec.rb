require "rails_helper"

describe JobsController do
  describe "#show" do
    xit "when a job is not found" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :show, id: 1
      expect(response).to be_success
      expect(response).to eq("erg")
    end
  end
end
