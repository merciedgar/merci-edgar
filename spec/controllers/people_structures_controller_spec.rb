require "rails_helper"

describe PeopleStructuresController do
  describe "#create" do
    it "works" do
      account = FactoryGirl.create(:account)
      contact = FactoryGirl.create(:contact, account_id: account.id)
      person = FactoryGirl.create(:person, name: 'Henri Ford', account_id: account.id, contact: contact)
      contact = FactoryGirl.create(:contact, account_id: account.id)
      structure = FactoryGirl.create(:structure, account_id: account.id, contact: contact)
      people_structure = FactoryGirl.create(:people_structure, person: person, structure: structure)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      post :create, structure_id: structure.id, person_name: 'Ford', format: 'js'

      expect(response).to be_success
    end
  end
end
