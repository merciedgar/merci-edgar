require 'rails_helper'

describe ShowBuyersController do
  describe "#index" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      show_buyer = FactoryGirl.create(:show_buyer, account_id: account.id)
      sign_in manager

      get :index

      expect(response).to be_success
      expect(assigns(:current_account)).to eq(account)
      expect(assigns(:contacts)).to eq([show_buyer])
    end
  end

  describe "#new" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      get :new, format: :json

      expect(response).to be_success
      expect(assigns(:current_account)).to eq(account)
    end
  end

  describe "#edit" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager
      contact = FactoryGirl.create(:contact, account_id: account.id)
      show_buyer = FactoryGirl.create(:show_buyer, account_id: account.id, structure: FactoryGirl.create(:structure, account_id: account.id, contact: contact))

      get :edit, id: show_buyer.id

      expect(response).to be_success
      expect(assigns(:current_account)).to eq(account)
      expect(assigns(:show_buyer)).to eq(show_buyer)
    end
  end

  describe "#show" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      contact = FactoryGirl.create(:contact)
      sign_in manager

      contact = FactoryGirl.create(:contact, account_id: account.id)
      show_buyer = FactoryGirl.create(:show_buyer, account_id: account.id, structure: FactoryGirl.create(:structure, account_id: account.id, contact: contact))

      get :show, id: show_buyer.id

      expect(response).to be_success
      expect(assigns(:current_account)).to eq(account)
      expect(assigns(:show_buyer)).to eq(show_buyer)
    end
  end

  describe "#update" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      contact = FactoryGirl.create(:contact)
      sign_in manager

      contact = FactoryGirl.create(:contact, account_id: account.id)
      show_buyer = FactoryGirl.create(:show_buyer, account_id: account.id, structure: FactoryGirl.create(:structure, account_id: account.id, contact: contact))

      post :update, id: show_buyer.id

      expect(response).to be_redirect
    end
  end

  describe "#destroy" do
    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      contact = FactoryGirl.create(:contact)
      sign_in manager

      contact = FactoryGirl.create(:contact, account_id: account.id)
      show_buyer = FactoryGirl.create(:show_buyer, account_id: account.id, structure: FactoryGirl.create(:structure, account_id: account.id, contact: contact))

      delete :destroy, id: show_buyer.id

      expect(response).to be_redirect
    end
  end
end

