require "rails_helper"

describe ReportingsController do
  describe "#create" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      contact = FactoryGirl.create(:person, account_id: account.id)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      post :create, format: 'js', reporting: {note_report_content: "", asset_id: contact.id, asset_type: "Person", project_id: ""}

      expect(response).to be_redirect
    end
  end

  describe "#update" do
    it "works well with a manager with the account assigns" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user, account: account, manager: true)
      @request.host = "#{account.domain}.lvh.me"
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in manager

      reporting = FactoryGirl.create(:reporting)

      post :update, id: reporting.id, format: :js

      expect(response).to be_success
    end
  end


end

