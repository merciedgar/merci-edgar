require "rails_helper"

describe Venue do

  it "have a valid factory" do
    expect(FactoryGirl.build(:venue)).to be_valid
  end

  describe "elements_to_export" do

    it "with an account with a person" do
      account = FactoryGirl.create(:account)
      Account.current_id = account.id
      venue = FactoryGirl.create(:venue, account: account)
      room = FactoryGirl.create(:room, venue: venue)

      expect(Venue.elements_to_export(account)).to eq([room])
    end
  end

  describe "capacity_tags" do

    it "without room" do
      account = FactoryGirl.create(:account)
      Account.current_id = account.id
      venue = FactoryGirl.create(:venue, account_id: account.id)
      expect(venue.capacity_tags).to eq([])
    end

    context "with just a room" do
      it "with 45 seats and 200 standing" do
        account = FactoryGirl.create(:account)
        Account.current_id = account.id
        venue = FactoryGirl.create(:venue, account_id: account.id)
        room = FactoryGirl.create(:room, seating: 45, standing: 200, venue_id: venue.id)
        expect(room.venue.capacity_tags).to eq(['< 100', '100-400'])
      end

      it "with 4500 seats and 0 standing" do
        account = FactoryGirl.create(:account)
        Account.current_id = account.id
        venue = FactoryGirl.create(:venue, account_id: account.id)
        room = FactoryGirl.create(:room, seating: 4500, standing: 0, venue_id: venue.id)
        expect(room.venue.capacity_tags).to eq(['> 1200'])
      end

      it "with 4500 seats and nil standing" do
        account = FactoryGirl.create(:account)
        Account.current_id = account.id
        venue = FactoryGirl.create(:venue, account_id: account.id)
        room = FactoryGirl.create(:room, seating: 4500, standing: nil, venue_id: venue.id)
        expect(room.venue.capacity_tags).to eq(['> 1200'])
      end

      it "with nil seats and nil standing" do
        account = FactoryGirl.create(:account)
        Account.current_id = account.id
        venue = FactoryGirl.create(:venue, account_id: account.id)
        room = FactoryGirl.create(:room, seating: nil, standing: nil, venue_id: venue.id)
        expect(room.venue.capacity_tags).to eq([])
      end

      it "with 423 seats and 1500 standing" do
        account = FactoryGirl.create(:account)
        Account.current_id = account.id
        venue = FactoryGirl.create(:venue, account_id: account.id)
        room = FactoryGirl.create(:room, seating: 423, standing: 1500, venue_id: venue.id)
        expect(room.venue.capacity_tags).to eq(['401-1200', '> 1200'])
      end
    end

    it "with 2 rooms" do
      account = FactoryGirl.create(:account)
      Account.current_id = account.id
      venue = FactoryGirl.create(:venue, account_id: account.id)
      first_room = FactoryGirl.create(:room, seating: 423, standing: 1500, venue_id: venue.id)
      second_room = FactoryGirl.create(:room, seating: 0, standing: 500, venue_id: venue.id)
      expect(first_room.venue.capacity_tags).to eq(['401-1200', '> 1200'])
    end
  end

  describe "translated_kind" do
    it "no specified kind" do
      account = FactoryGirl.create(:account)
      Account.current_id = account.id
      venue = FactoryGirl.build(:venue, kind: nil)
      expect(venue.translated_kind).to eq(nil)
    end

    it "with kind" do
      account = FactoryGirl.create(:account)
      Account.current_id = account.id
      venue = FactoryGirl.build(:venue, kind: :cultural_center)
      expect(venue.translated_kind).to eq(I18n.t(venue.kind, scope: 'simple_form.options.venue.kind'))
    end
  end

  describe "#to_s" do
    it "works" do
      venue = FactoryGirl.build(:venue)
      expect(venue.to_s).to eq(venue.name)
    end
  end

  describe "#capacities" do
    it "works" do
      venue = FactoryGirl.create(:venue)
      capacity = FactoryGirl.create(:capacity, nb: 12)
      room = FactoryGirl.create(:room, venue: venue, capacities: [capacity])
      expect(venue.capacities).to eq([])
    end
  end

  describe "#season" do
    it "non renseigné without informations" do
      venue = FactoryGirl.build(:venue)
      expect(venue.season).to eq("Non renseigné")
    end
  end

  describe "#services" do
    it "non renseigné without informations" do
      venue = FactoryGirl.build(:venue)
      expect(venue.services).to eq("Non renseigné")
    end
  end

  describe "#contract_list" do
    it "empty without informations" do
      venue = FactoryGirl.build(:venue)
      expect(venue.contract_list).to eq([])
    end
  end

  describe ".from_csv" do
    it "works" do
      row = {
        nom: "Bla",
        email: "",
        tel: "",
        adresse: "",
        complement_d_adresse: "",
        code_postal: "",
        ville: "",
        pays: "",
        web: "",
        type: "",
        lieu: "",
        residence: "",
        accompagnement: "",
        reseaux: "",
        tags_perso: "",
        saison: "",
        styles: "",
        contrats: "",
        decouverte: "",
        cycle_de_programmation: "",
        observations_programmation: "",
        mois_prospection: "",
        places_assises: "",
        places_debout: "",
        places_mixte: "",
        places_debout_modulable: "",
        places_assises_modulable: "",
        ouverture_plateau: "",
        profondeur_plateau: "",
        hauteur_plateau: "",
        bar_salle: "",
        observations: "",
        nom_programmateur: "Henri",
        email_programmateur: "",
        tel_programmateur: "",
        observations_programmateur: "",
        nom_regisseur: "Stéphane",
        email_regisseur: "",
        tel_regisseur: "",
        observations_regisseur: "",
        nom_responsable_communication: "",
        tel_responsable_communication: ""
      }
      result = Venue.from_csv(row)
      venue = result[0]
      invalid_keys = result[1]
      expect(venue).to be_a(Venue)
      expect(invalid_keys).to eq(["complement_d_adresse", "type", "lieu", "decouverte", "places_mixte", "places_debout_modulable", "places_assises_modulable", "ouverture_plateau", "profondeur_plateau", "hauteur_plateau", "nom_responsable_communication", "tel_responsable_communication", "cycle_de_programmation"])
    end
  end

  describe ".csv_header" do
    it "works" do
      expected = "Nom, Emails, Tels, Adresses, Sites web, Type, Residence, Accompagnement, Réseaux, Tags perso, Saison, Style, Contrats, Découverte, Cycle de programmation, Observations Programmations, Mois de prospection, Observations, Nom Salle, Places assises, Places debout, Modulable, Dimension Plateau (PxLxH), Bar, Personnes\n"
      expect(Venue.csv_header).to eq(expected)
    end
  end

  describe ".export_filename" do
    it "works" do
      account = FactoryGirl.build(:account)
      expect(Venue.export_filename(account)).to eq("lieux-#{account.domain}.csv")
    end
  end

  describe "#discovery" do
    it "nil without informations" do
      venue = FactoryGirl.build(:venue)
      expect(venue.period).to be_nil
    end
  end

  describe "#period" do
    it "nil without informations" do
      venue = FactoryGirl.build(:venue)
      expect(venue.period).to be_nil
    end
  end

  describe "#translated_period" do
    it "nil without informations" do
      venue = FactoryGirl.build(:venue)
      expect(venue.translated_period).to be_nil
    end
  end

  describe "#scheduling_remark" do
    it "nil without informations" do
      venue = FactoryGirl.build(:venue)
      expect(venue.scheduling_remark).to be_nil
    end
  end

  describe "#prospecting_months" do
    it "nil without informations" do
      venue = FactoryGirl.build(:venue)
      expect(venue.prospecting_months).to be_nil
    end
  end

end
