require "rails_helper"

describe Room do
  it "have valid factory" do
    expect(FactoryGirl.build(:room)).to be_valid
  end

  it "need venue_id to be valid" do
    expect(FactoryGirl.build(:room, venue_id: nil)).to be_invalid
  end

  describe "#to_csv" do
    it "works" do
      venue = FactoryGirl.build(:venue, kind: :cultural_center)
      room = FactoryGirl.build(:room, venue: venue)

      expected_line = [
        room.venue.name, ExportTools.build_list(room.venue.emails), ExportTools.build_list(room.venue.phones),
        ExportTools.build_list(room.venue.addresses), ExportTools.build_list(room.venue.websites),
        room.venue.translated_kind, room.venue.residency, room.venue.accompaniment,
        room.venue.network_list, room.venue.custom_list,
        room.venue.season_months, room.venue.style_list,
        room.venue.contract_list, room.venue.discovery,
        room.venue.translated_period, room.venue.scheduling_remark,
        room.venue.prospecting_months, room.venue.remark,
        room.name, room.seating, room.standing, room.modular_space,
        "#{room.depth} x #{room.width} x #{room.height}", room.bar,
        ExportTools.build_list(room.venue.people)
      ].to_csv

      expect(room.to_csv).to eq(expected_line)
    end
  end

  describe "#from_csv" do
    it "work" do
      room = Room.from_csv({
        nom: "un nom",
        places_debout: 3,
        places_assises: 0
      })

      expect(room.bar).to be_nil
      expect(room.width).to eq(nil)
      expect(room.height).to eq(nil)
      expect(room.depth).to eq(nil)
    end
  end
end
