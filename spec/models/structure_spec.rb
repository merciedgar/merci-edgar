require "rails_helper"

describe Structure do

  describe "have validations and a valid factory" do
    it "have a valid factory" do
      expect(FactoryGirl.build(:structure)).to be_valid
    end

    it "have a valid factory, when generic" do
      expect(FactoryGirl.build(:structure, :generic)).to be_valid
    end
  end

  describe "elements_to_export" do
    context "with an account with a generic structure" do
      it "build a structures filename" do
        account = FactoryGirl.create(:account)
        structure = FactoryGirl.create(:structure, :generic, account_id: account.id)
        expect(Structure.export_filename(account)).to eq("structures-#{account.domain}.csv")
      end

      it "export an array with structure" do
        account = FactoryGirl.create(:account)
        structure = FactoryGirl.create(:structure, :generic, account_id: account.id)
        expect(Structure.elements_to_export(account)).to eq([structure])
      end
    end
  end

  describe "to_csv" do
    context "with a generic structure" do
      it "return a well formated csv" do
        structure = FactoryGirl.create(:structure, :generic)
        expected_line =[
          structure.to_s,
          ExportTools.build_list(structure.emails),
          ExportTools.build_list(structure.phones),
          ExportTools.build_list(structure.addresses),
          ExportTools.build_list(structure.websites),
          structure.network_list,
          structure.custom_list,
          structure.remark,
          ExportTools.build_list(structure.people)
        ].to_csv
        expect(structure.to_csv).to eq(expected_line)
      end
    end

    context "with a venue" do
      it "return structure csv line" do
        venue = FactoryGirl.create(:venue)
        structure = venue.structure
        expected_line = [
          structure.to_s,
          ExportTools.build_list(structure.emails),
          ExportTools.build_list(structure.phones),
          ExportTools.build_list(structure.addresses),
          ExportTools.build_list(structure.websites),
          structure.network_list,
          structure.custom_list,
          structure.remark,
          ExportTools.build_list(structure.people)
        ].to_csv
        expect(structure.to_csv).to eq(expected_line)
      end
    end

  end

  describe "to_s" do
    context "with a venue" do
      it "return « Lieu » with name" do
        contact = FactoryGirl.create(:contact, name: 'La Clef')
        venue = FactoryGirl.create(:structure, contact: contact, structurable_type: :venue )
        expect(venue.to_s).to eq('La Clef [Lieu]')
      end
    end

    context "with a generic structure" do
      it "return « structure générique » in name" do
        contact = FactoryGirl.create(:contact, name: 'La Clef')
        structure = FactoryGirl.create(:structure, contact: contact, structurable_type: nil )
        expect(structure.to_s).to eq('La Clef [Structure générique]')
      end
    end
  end

  describe "#search_for" do
    it "bring empty array when no structure founded" do
      expect(Structure.search_for("something")).to eq([])
    end

    it "bring structure when contact name mathce term" do
      contact = FactoryGirl.create(:contact, name: "Henri Des")
      structure = FactoryGirl.create(:structure, contact: contact)
      other_contact = FactoryGirl.create(:contact, name: "Other")
      FactoryGirl.create(:structure, contact: other_contact)

      expect(Structure.search_for("henri")).to eq([structure])
    end

    it "bring structure when contact name mathce term" do
      contact = FactoryGirl.create(:contact, name: "Henri Des")
      structure = FactoryGirl.create(:structure, contact: contact)
      expect(Structure.search_for("Henri")).to eq([structure])
    end
  end

  describe "#add_person" do

    it "work" do
      structure = FactoryGirl.create(:structure)
      structure.add_person("Steve", "Dindon", "Empereur")
      expect(structure.people_structures.first.person.first_name).to eq("Steve")
      expect(structure.people_structures.first.person.last_name).to eq("Dindon")
      expect(structure.people_structures.first.title).to eq("Empereur")
    end
  end

  describe "#main_person" do

  end

  describe "#main_person?" do
    it "return false when..." do
      contact = FactoryGirl.create(:contact, name: "Henri Des")
      other_contact = FactoryGirl.create(:contact, name: "Christophe")
      structure = FactoryGirl.create(:structure, contact: contact)
      expect(structure.main_person?(contact, other_contact)).to be_falsy
    end

    it "return true when..." do
      contact = FactoryGirl.create(:contact, name: "Henri Des")
      someone = FactoryGirl.create(:person)
      structure = FactoryGirl.create(:structure, contact: contact, people: [someone] )
      expect(structure.main_person?(contact, someone)).to be_truthy
    end
  end

  describe "#kind=" do
    it "can be a venue" do
      structure = FactoryGirl.create(:structure)
      structure.kind = "venue"
      expect(structure.structurable).to be_a(Venue)
    end

    it "can be a festival" do
      structure = FactoryGirl.create(:structure)
      structure.kind = "festival"
      expect(structure.structurable).to be_a(Festival)
    end

    it "can be a show_buyer" do
      structure = FactoryGirl.create(:structure)
      structure.kind = "show_buyer"
      expect(structure.structurable).to be_a(ShowBuyer)
    end
  end

  describe ".from_csv" do
    it "works" do
      row = {
        nom: "Henri",
        emails: "henri@ford.com",
        téls: "",
        adresses: "",
        sites_web: "",
        réseaux: "",
        tag_perso: "",
        commentaires: "",
        personnes: ""
      }
      result = Structure.from_csv(row)
      expect(result[0]).to be_a(Structure)
      expect(result[1]).to eq(["emails", "téls", "adresses", "sites_web", "réseaux", "tag_perso", "commentaires", "personnes"])
    end
  end

  describe ".csv_header" do
    it "return well formatted CSV header" do
      expect(Structure.csv_header).to eq("Nom,Emails,Téls,Adresses,Sites web,Réseaux,Tag Perso,Commentaires,Personnes\n")
    end
  end
end
