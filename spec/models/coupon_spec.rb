require "rails_helper"

describe Coupon do
  it "have a valid factory" do
    expect(FactoryGirl.build(:coupon)).to be_valid
  end

  describe "#set_code" do
    it "works" do
      coupon = FactoryGirl.build(:coupon, code: nil)
      coupon.set_code
      expect(coupon.code).to_not be_nil
    end
  end

  describe "#status" do
    it "a distribuer without account" do
      coupon = FactoryGirl.build(:coupon, code: nil)
      expect(coupon.status).to eq("A distribuer")
    end

    it "works" do
      account = FactoryGirl.create(:account, name: "Truc")
      coupon = FactoryGirl.build(:coupon, code: nil, account: account)
      expect(coupon.status).to eq("Truc")
    end
  end
end
