require "rails_helper"

describe Network do

  describe "#add_network" do
    it "works" do
      Network.add_network("some")
      expect(Network.first.network).to eq("some")
    end
  end

  describe "#add_networks" do
    it "works" do
      Network.add_networks(["some", "thing"])
      expect(Network.all.map(&:network).sort).to eq(["some", "thing"].sort)
    end
  end

end
