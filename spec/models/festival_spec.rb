require 'rails_helper'

describe Festival do

  it "have a valid factory" do
    expect(FactoryGirl.build(:festival)).to be_valid
  end

  describe :fine_model do
    it "works" do
      festival = Festival.new
      expect(festival.fine_model).to eq(festival)
    end
  end

  describe ".from_csv" do
    it "works" do
      row = {
        nom: "MAINS D'ŒUVRES",
        email: "info@mainsdoeuvres.org",
        tel: "01 40 11 25 25 ",
        adresse: "1 RUE CHARLES GARNIER",
        complement_d_adresse: "",
        code_postal: "",
        ville: "ST OUEN",
        pays: "",
        web: "www.mainsdoeuvres.org",
        reseaux: "",
        tags_perso: "",
        nom_secretaire: "GILLES MORDANT",
        email_secretaire: "gillou@mourdant.org",
        tel_secretaire: "123",
        observations_secretaire: "N'a pas d'oreille …",
        nb_editions: "3",
        derniere_annee: "2010",
        styles: "PLURIDISCIPLINAIRE",
        contrats: "bilibop",
        decouverte: "yupee",
        cycle_de_programmation: "12",
        observations_programmation: "",
        mois_prospection: "1..3"
      }
      festival = Festival.from_csv(row)[0]
      expect(festival).to be_a(Festival)
      expect(festival.nb_edition).to eq(3)
      expect(festival.last_year).to eq(2010)
    end
  end

end
