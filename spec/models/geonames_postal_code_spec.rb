require "rails_helper"

describe GeonamesPostalCode do

  describe "#get_latitude_longitude_and_admin_names(options)" do

    it "return nil when nothing given" do
      3.times {FactoryGirl.create(:geonames_postal_code)}
      result = GeonamesPostalCode.get_latitude_longitude_and_admin_names({})
      expect(result).to be_nil
    end

    it "return latitude and longitude when something given" do
      premiere = FactoryGirl.create(:geonames_postal_code, place_name: "Paris", postal_code: "75012")
      deuxieme = FactoryGirl.create(:geonames_postal_code, place_name: "Lyon", postal_code: "69001")
      troisieme = FactoryGirl.create(:geonames_postal_code, place_name: "Marseille", postal_code: "13004")
      result = GeonamesPostalCode.get_latitude_longitude_and_admin_names({city: premiere.place_name, postal_code: premiere.postal_code})

      expect(result["latitude"]).to eq(premiere.latitude)
      expect(result["longitude"]).to eq(premiere.longitude)
    end

  end

end

