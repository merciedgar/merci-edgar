require "rails_helper"

describe Address do

  it "have validations and a valid factory" do
    expect(FactoryGirl.build(:address)).to be_valid
  end


  describe :full_address do
    it "return a string with all data when exist" do
      address = FactoryGirl.build(:address,
                                  street: '34 rue de la gare',
                                  city: 'Marseille',
                                  postal_code: '13003',
                                  more_info: 'près du piano, à coté du McDo',
                                  country: 'France')
      expect(address.full_address).to eq('34 rue de la gare, 13003 Marseille, France, près du piano, à coté du McDo')
    end

    it "return string only with existing data" do
      address = FactoryGirl.build(:address,
                                  street: nil,
                                  city: 'Marseille',
                                  postal_code: '13003',
                                  more_info: nil,
                                  country: 'France')
      expect(address.full_address).to eq('13003 Marseille, France')
    end
 
  end

  describe :to_s do
    it "return string based on full_address and add kind of address" do
      address = FactoryGirl.build(:address, street: '34 rue de la gare', city: 'Marseille', postal_code: '13003', more_info: 'près du piano, à coté du McDo', country: 'FR', kind: Address::MAIN_ADDRESS)
      expect(address.to_s).to eq("#{address.full_address} [Adresse principale]")
    end

    it 'return only full address without kind' do
      address = FactoryGirl.build(:address, street: '34 rue de la gare', city: 'Marseille', postal_code: '13003', more_info: 'près du piano, à coté du McDo', country: 'FR', kind: nil)
      expect(address.to_s).to eq("#{address.full_address}")
    end
  end

  describe "#address_for_geocode" do
    it "works" do
      address = FactoryGirl.build(:address, postal_code: "92000", city: "Nanterre" )
      expect(address.address_for_geocode).to eq("Rue du Test, 92000 Nanterre, France")
    end
  end

  describe "#department_name" do
    it "works" do
      address = FactoryGirl.build(:address, postal_code: "92000" )
      expect(address.department_name).to eq("Hauts de Seine")
    end
  end

  describe "#region_code" do
    it "#works" do
      address = FactoryGirl.build(:address, postal_code: "92000" )
      expect(address.region_code).to eq(10)
    end
  end

  describe "#region_name" do
    it "works" do
      address = FactoryGirl.build(:address, postal_code: "92000" )
      expect(address.region_name).to eq("Ile de France")
    end
  end

  describe "#department_code" do
    it "works" do
      address = FactoryGirl.build(:address, postal_code: "92000" )
      expect(address.department_code).to eq("92")
    end
  end

  describe "#gmaps4rails_infowindow" do
    it "works" do
      address = FactoryGirl.build(:address, contact: FactoryGirl.create(:contact, name: "Henry"))
      expect(address.gmaps4rails_infowindow).to eq("Henry")
    end
  end

  describe "#format_postal_code" do
    it "works" do
      address = FactoryGirl.build(:address, city: "Saint-Quentin", postal_code: "2000")
      expect(address.format_postal_code).to eq("02000")
    end
  end

end
