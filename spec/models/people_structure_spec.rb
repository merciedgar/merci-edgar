require "rails_helper"

describe PeopleStructure do

  it "have a valid factory" do
    expect(FactoryGirl.build(:people_structure)).to be_valid
  end

  describe "#structure_name" do
    it "works" do
      people_structure = FactoryGirl.build(:people_structure)
      expect(people_structure.structure_name).to eq(people_structure.structure.name)
    end
  end

  describe "#structure_name=" do
    it "works" do
      people_structure = FactoryGirl.build(:people_structure)
      people_structure.structure_name = "Truc"
      expect(people_structure.structure_name).to eq("Truc")
    end
  end

  describe "#to_s" do
    it "works" do
      people_structure = FactoryGirl.build(:people_structure, title: "Boss")
      people_structure.structure_name = "BigCompany"
      expect(people_structure.to_s).to eq("Boss à Bigcompany")
    end
  end
end
