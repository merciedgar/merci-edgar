require "rails_helper"
require "tempfile"

describe SpreadsheetFile do

  it "truthy with a authorized encoded file" do
    file = Tempfile.new("fakefile")
    File.open(file, 'w') { |f| f.puts "something strange" }
    spreadsheet = SpreadsheetFile.new(file.path)
    expect(spreadsheet.readable?).to be_truthy
  end

  it "falsy with a unauthorized (binary) encoded file" do
    file = Tempfile.new("fakefile")
    spreadsheet = SpreadsheetFile.new(file.path)
    expect(spreadsheet.readable?).to be_falsy
  end

  describe "#name_header_exist" do
    it "return true if first line contains header" do
      file = Tempfile.new("fakefile")
      File.open(file, 'w') do |f|
        f.puts "something,nom"
      end
      spreadsheet = SpreadsheetFile.new(file.path)
      expect(spreadsheet.name_header_exist).to be_truthy
    end

    it "return false if first line contains header" do
      file = Tempfile.new("fakefile")
      File.open(file, 'w') do |f|
        f.puts "something,here"
      end
      spreadsheet = SpreadsheetFile.new(file.path)
      expect(spreadsheet.name_header_exist).to be_falsy
    end

  end

  describe "#nb_lines" do
    it "works" do
      file = Tempfile.new("fakefile")
      File.open(file, 'w') { |f| f.puts "just a word" }
      spreadsheet = SpreadsheetFile.new(file.path)
      expect(spreadsheet.nb_lines).to eq(1)
    end
  end

  describe "#to_csv" do
    it "works" do
      spreadsheet = SpreadsheetFile.new(__FILE__)
      expect(spreadsheet.to_csv).to eq(__FILE__)
    end
  end

  describe "#file_encoding" do
    it "works" do
      spreadsheet = SpreadsheetFile.new(__FILE__)
      expect(spreadsheet.file_encoding).to eq("us-ascii")
    end
  end
end

