require 'rails_helper'

describe Contact do

  it "have a valid factory" do
    expect(FactoryGirl.build(:contact)).to be_valid
  end

  describe "delete_after_store" do
    it "with a phone" do
      contact = FactoryGirl.build(:contact)
      phone = contact.phones.build(number: '12 xx xx')
      contact.delete_after_store!(phone)
      expect(contact.remark).to eq("\nTel: #{phone.number} /")
      expect(contact.phones).to eq([])
    end

    it "with an email" do
      contact = FactoryGirl.build(:contact)
      email = contact.emails.build(address: 'trucucu')
      contact.delete_after_store!(email)
      expect(contact.remark).to eq("\nEmail: #{email.address} /")
      expect(contact.emails).to eq([])
    end

    it "with an website" do
      contact = FactoryGirl.build(:contact)
      website = contact.websites.build(url: 'http://mywebsite.com')
      contact.delete_after_store!(website)
      expect(contact.remark).to eq("\nSite: #{website.url} /")
      expect(contact.websites).to eq([])
    end
  end

  describe "assign_from_csv" do
    it "empty when nothing given" do
      contact = FactoryGirl.build(:contact)
      expect(contact.assign_from_csv({})).to eq([])
    end

    it "works" do
      contact = FactoryGirl.build(:contact)
      expect(contact.assign_from_csv({tel: '12 23 xx 45 56'})).to eq([])
    end
  end

  describe "#dup_id" do
    it "works" do
      contact = FactoryGirl.create(:contact)
      other_contact = FactoryGirl.create(:contact, duplicate_id: contact.id)
      expect(contact.dup_id).to eq(contact.id)
    end
  end

  describe "#phone_number" do
    it "works" do
      contact = FactoryGirl.build(:contact, phones: [FactoryGirl.create(:phone, number: "322323")])
      expect(contact.phone_number).to eq("+32 2 323")
    end
  end

  describe "#email_address" do
    it "works" do
      contact = FactoryGirl.build(:contact, emails: [FactoryGirl.create(:email, address: "truc@laposte.net")])
      expect(contact.email_address).to eq("truc@laposte.net")
    end
  end

  describe "#postal_code" do
    it "works" do
      contact = FactoryGirl.build(:contact, addresses: [FactoryGirl.create(:address, postal_code: "75012")])
      expect(contact.postal_code).to eq("75012")
    end
  end

  describe "#country" do
    it "works" do
      contact = FactoryGirl.build(:contact, addresses: [FactoryGirl.create(:address, country: "france")])
      expect(contact.country).to eq("france")
    end
  end

  describe "#website_url" do
    it "works" do
      contact = FactoryGirl.build(:contact, websites: [FactoryGirl.create(:website, url: "https://example.org")])
      expect(contact.website_url).to eq("https://example.org")
    end
  end

  describe ".by_type" do
    it "invalid" do
      expect{
        Contact.by_type("truc")
      }.to raise_error(RuntimeError, "Invalid Parameter")
    end

    it "venues" do
      venue = FactoryGirl.create(:venue)
      expect(Contact.by_type("venues")).to eq([venue.contact])
    end

    it "festivals" do
      festival = FactoryGirl.create(:festival)
      expect(Contact.by_type("festivals")).to eq([festival.contact])
    end

    it "show_buyers" do
      show_buyer = FactoryGirl.create(:show_buyer)
      expect(Contact.by_type("show_buyers")).to eq([show_buyer.contact])
    end

    it "structures" do
      structure = FactoryGirl.create(:structure)
      expect(Contact.by_type("structures")).to eq([structure.contact])
    end

    it "people" do
      person = FactoryGirl.create(:person)
      expect(Contact.by_type("people")).to eq([person.contact])
    end

  end

  describe "#favorite?" do

    it "return false " do
      user = FactoryGirl.create(:user)
      other_user = FactoryGirl.create(:user)
      contact = FactoryGirl.create(:contact)
      FactoryGirl.create(:favorite_contact, user: other_user, contact: contact)
      expect(contact.favorite?(user)).to be_falsy
    end

    it "return true " do
      user = FactoryGirl.create(:user)
      contact = FactoryGirl.create(:contact)
      FactoryGirl.create(:favorite_contact, user: user, contact: contact)
      expect(contact.favorite?(user)).to be_truthy
    end

  end

  describe "#custom_list" do
    it "nil when nothing custom" do
      contact = FactoryGirl.create(:contact)
      expect(contact.custom_list).to eq(nil)
    end

    it "tags custom" do
      contact = FactoryGirl.create(:contact, custom_tags: "truc, bidule")
      expect(contact.custom_list).to eq(["truc", "bidule"])
    end
  end

  describe "#custom_list=" do
    it "nil when nothing custom" do
      contact = FactoryGirl.create(:contact)
      contact.custom_list = nil
      expect(contact.custom_list).to eq(nil)
    end

    it "add tag to custom" do
      contact = FactoryGirl.create(:contact)
      contact.custom_list = ["truc"]
      expect(contact.custom_list).to eq(["truc"])
    end
  end

  describe "#network_list" do
    it "nil when no network" do
      contact = FactoryGirl.create(:contact)
      expect(contact.network_list).to eq(nil)
    end

    it "network list" do
      contact = FactoryGirl.create(:contact, network_tags: "truc, bidule")
      expect(contact.network_list).to eq(["truc", "bidule"])
    end
  end

  describe "#network_list=" do
    it "nil when no network" do
      contact = FactoryGirl.create(:contact)
      contact.network_list = nil
      expect(contact.network_list).to eq(nil)
    end

    it "add network" do
      contact = FactoryGirl.create(:contact)
      contact.network_list = ["truc"]
      expect(contact.network_list).to eq(["truc"])
    end
  end

  describe ".tagged_with" do
    it "empty without mathing" do
      expect(Contact.tagged_with(Contact, "truc, bidule", "network_tags")).to eq([])
    end

    it "works" do
      FactoryGirl.create(:contact)
      bidule = FactoryGirl.create(:contact, network_list: ["bidule"])
      FactoryGirl.create(:contact)
      truc= FactoryGirl.create(:contact, network_list: ["truc"])
      FactoryGirl.create(:contact)
      expect(Contact.tagged_with(Contact, "truc, bidule", "network_tags").sort).to eq([truc, bidule].sort)
    end
  end

  describe ".in_string_list" do
    it "empty" do
      expect(Contact.in_string_list(Contact, "truc, bidule", "network_tags")).to eq([])
    end

    it "empty" do
      FactoryGirl.create(:contact)
      bidule = FactoryGirl.create(:contact, network_list: ["bidule"])
      FactoryGirl.create(:contact)
      truc= FactoryGirl.create(:contact, network_list: ["truc"])
      FactoryGirl.create(:contact)
      expect(Contact.in_string_list(Contact, "truc, bidule", "network_tags").sort).to eq([truc, bidule].sort)
    end
  end

  describe "#update_networks" do
    it "works" do
      contact = FactoryGirl.build(:contact, network_tags: ["truc", "chose"])
      contact.update_networks
      expect(Network.count).to eq(1)
    end
  end

  describe "#format_networks" do
    it "works" do
      contact = FactoryGirl.build(:contact, network_tags: "  Truc,   bidule, chose")
      contact.format_networks
      expect(contact.network_tags).to eq("truc,bidule,chose")
    end
  end

  describe ".get_or_init_by_name" do
    it "create new new none exist" do
      new_contact = Contact.get_or_init_by_name("Henri", "20180702", true)
      expect(new_contact).to be_a(Contact)
      expect(new_contact.name).to eq("Henri")
    end

    it "get existing one" do
      FactoryGirl.create(:contact, name: "Henri")
      new_contact = Contact.get_or_init_by_name("Henri", "20180702", true)
      expect(new_contact).to be_a(Contact)
      expect(new_contact.name).to eq("Henri #1")
    end
  end

  describe "#build_list" do
    it "works" do
      contact = FactoryGirl.build(:contact)
      expect(contact.build_list("truc, bidule")).to eq("truc,bidule")
    end
  end
end
