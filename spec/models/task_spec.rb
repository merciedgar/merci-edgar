require "rails_helper"

describe Task do

  it "have validations and a valid factory" do
    expect(FactoryGirl.build(:task)).to be_valid
  end

  describe "#friendly_date with a not completed tasks" do
    it "En retard" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      ActiveSupport::TimeZone.stubs(:now).returns(date)
      Date.stubs(:current).returns(date)
      DateTime.stubs(:now).returns(date)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, completed_at: nil,  due_at: (date - 2.hours))
      expect(task.friendly_date).to eq(["black", "En retard // 13 nov. 12:56"])
    end

    it "Aujourd'hui" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      ActiveSupport::TimeZone.stubs(:now).returns(date)
      Date.stubs(:current).returns(date)
      DateTime.stubs(:now).returns(date)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, completed_at: nil, due_at: (date + 4.hours) )
      expect(task.friendly_date).to eq(["red", "Aujourd'hui"])
    end

    it "Demain" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      ActiveSupport::TimeZone.stubs(:now).returns(date)
      Date.stubs(:current).returns(date)
      DateTime.stubs(:now).returns(date)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, completed_at: nil, due_at: (date + 1.day + 4.hours) )
      expect(task.friendly_date).to eq(["blue", "Demain"])
    end

    it "La semaine prochaine" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      ActiveSupport::TimeZone.stubs(:now).returns(date)
      Date.stubs(:current).returns(date)
      DateTime.stubs(:now).returns(date)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, completed_at: nil, due_at: (date + 4.day + 4.hours) )
      expect(task.friendly_date).to eq(["gray", "La semaine prochaine"])
    end


    describe "#friendly_date with completed tasks" do
      it "works without due date" do
        date = DateTime.new(2015, 11, 13, 13, 56)
        ActiveSupport::TimeZone.stubs(:now).returns(date)
        Date.stubs(:current).returns(date)
        DateTime.stubs(:now).returns(date)
        Time.stubs(:now).returns(date)
        task = FactoryGirl.build(:task, completed_at: date - 3.hours, due_at: date - 2.hours)
        expect(task.friendly_date).to eq(["green", "13 nov. 11:56"])
      end
    end
  end

  describe "#assign_to" do
    it "works" do
      task = FactoryGirl.build(:task)
      user = FactoryGirl.build(:user)
      expect(task.assign_to(user)).to be_truthy
      expect(task.assignee).to eq(user)
    end
  end

  describe "#assignee_name" do
    it "return assignee name" do
      user = FactoryGirl.build(:user)
      task = FactoryGirl.build(:task, assignee: user)
      expect(task.assignee_name).to eq(user.name)
    end

    it "return nil when no assignee" do
      task = FactoryGirl.build(:task, assignee: nil)
      expect(task.assignee_name).to be_nil
    end
  end

  describe "#bucket" do
    it "return due_today when no due date" do
      task = FactoryGirl.build(:task, due_at: nil)
      expect(task.bucket).to eq("due_today")
    end

    it "return specific when specific_time" do
      task = FactoryGirl.build(:task, specific_time: true)
      expect(task.bucket).to eq("specific")
    end

    it "return overdue when due_at < now" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, specific_time: false, due_at: date - 1.day)
      expect(task.bucket).to eq("overdue")
    end

    it "return due_today when due_at for today" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, specific_time: false, due_at: date)
      expect(task.bucket).to eq("due_today")
    end

    it "return due_tomorrow when due_at for tomorrow" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, specific_time: false, due_at: date + 1.day)
      expect(task.bucket).to eq("due_tomorrow")
    end

    it "return due_this_week when due_at next week" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, specific_time: false, due_at: date + 2.day)
      expect(task.bucket).to eq("due_this_week")
    end

    it "return due_next_week when due_at for next week" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, specific_time: false, due_at: date + 8.day)
      expect(task.bucket).to eq("due_next_week")
    end

    it "return due_later when due_at is later" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, specific_time: false, due_at: date + 2.month)
      expect(task.bucket).to eq("due_later")
    end

  end

  describe "#bucket=" do

    it "set due_at to nil when not in list" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, due_at: date)
      task.bucket = "nothing"
      expect(task.due_at).to be_nil
    end

    it "set due_at to yesterday when overdue " do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, due_at: date)
      task.bucket = "overdue"
      expect(task.due_at).to be < date
    end

    it "set due_at to end of day when today " do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, due_at: date)
      task.bucket = "due_today"
      expect(task.due_at).to eq(Time.zone.now.end_of_day)
    end

    it "set due_at to tomorrow when due_tomorrow " do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, due_at: date)
      task.bucket = "due_tomorrow"
      expect(task.due_at).to eq(Time.zone.now.end_of_day.tomorrow)
    end

    it "set due_at to tomorrow when due_tomorrow " do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, due_at: date)
      task.bucket = "due_this_week"
      expect(task.due_at).to eq(Time.zone.now.end_of_day.end_of_week)
    end

    it "set due_at to tomorrow when due_tomorrow " do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, due_at: date)
      task.bucket = "due_next_week"
      expect(task.due_at).to eq(Time.zone.now.end_of_day.next_week.end_of_week)
    end

    it "set due_at to tomorrow when due_tomorrow " do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, due_at: date)
      task.bucket = "due_this_month"
      expect(task.due_at).to eq(Time.zone.now.end_of_day.end_of_month)
    end

    it "set due_at to tomorrow when due_tomorrow " do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      task = FactoryGirl.build(:task, due_at: date)
      task.bucket = "due_later"
      expect(task.due_at).to eq(Time.zone.now.midnight + 100.years)
    end

  end

  describe "#to_ics" do
    it "works with a due_at" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      task = FactoryGirl.create(:task, due_at: date)
      ics = task.to_ics
      expect(ics).to be_a(Icalendar::Event)
      expect(ics.dtstart).to eq(date + 1.hour)
    end
  end

  describe "#complete" do
    it "work" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      user = FactoryGirl.create(:user)
      task = FactoryGirl.create(:task)
      task.complete(user)
      expect(task.completor).to eq(user)
      expect(task.completed_at).to eq(date)
    end
  end

  describe "#uncomplete" do
    it "works" do
      date = DateTime.new(2015, 11, 13, 13, 56)
      Time.stubs(:now).returns(date)
      user = FactoryGirl.create(:user)
      task = FactoryGirl.create(:task, completor: user, completed_at: date)
      task.uncomplete
      expect(task.completor).to be_nil
      expect(task.completed_at).to be_nil
    end
  end

end

