require "rails_helper"

describe Scheduling do

  it "have validations and a valid factory" do
    expect(FactoryGirl.build(:scheduling)).to be_valid
  end

  describe "#full_name" do
    it "with a show buyer only" do
      buyer = FactoryGirl.build(:show_buyer)
      scheduling = FactoryGirl.build(:scheduling, show_buyer: buyer)
      expect(scheduling.full_name).to eq("#{scheduling.name} (#{buyer.name} [#{ShowBuyer.model_name.human}])")
    end

    it "with a festival only" do
      festival = FactoryGirl.build(:festival)
      scheduling = FactoryGirl.build(:scheduling, show_host: festival, show_buyer: nil)
      expect(scheduling.full_name).to eq("#{scheduling.name} (#{festival.name} [#{Festival.model_name.human}])")
    end

    it "with a venue only" do
      venue = FactoryGirl.build(:venue)
      scheduling = FactoryGirl.build(:scheduling, show_host: venue, show_buyer: nil)
      expect(scheduling.full_name).to eq("#{scheduling.name} (#{venue.name} [#{Venue.model_name.human}])")
    end

    it "with a venue and a show buyer" do
      venue = FactoryGirl.build(:venue)
      buyer = FactoryGirl.build(:show_buyer)
      scheduling = FactoryGirl.build(:scheduling, show_host: venue, show_buyer: buyer )
      expect(scheduling.full_name).to eq("#{scheduling.name} (#{buyer.name} [#{ShowBuyer.model_name.human}] => #{venue.name} [#{Venue.model_name.human}])")
    end
  end

  describe "translated_period" do

    it "no specified period" do
      scheduling = FactoryGirl.build(:scheduling, period: nil)
      expect(scheduling.translated_period).to eq(nil)
    end

    it "with period" do
      scheduling = FactoryGirl.build(:scheduling, period: Scheduling::QUATERLY)
      expect(scheduling.translated_period).to eq(I18n.t(scheduling.period, scope: 'simple_form.options.schedulings.period'))
    end
  end

  describe "elements_to_export" do
    it "with an account with a scheduling" do
      account = FactoryGirl.create(:account)
      buyer = FactoryGirl.create(:show_buyer, account_id: account.id)
      scheduling = FactoryGirl.create(:scheduling, show_buyer: buyer)

      expect(Scheduling.elements_to_export(account)).to eq([scheduling])
    end
  end

  describe "to_csv" do

    it "work with a show buyer and a period" do
      buyer = FactoryGirl.create(:show_buyer)
      scheduling = FactoryGirl.create(:scheduling, show_buyer: buyer, show_host: nil, period: Scheduling::QUATERLY)

      expected_line = [
        scheduling.full_name, scheduling.scheduler_name, I18n.t(scheduling.period, scope: 'simple_form.options.schedulings.period'), scheduling.prospecting_months, scheduling.contract_list, scheduling.style_list, scheduling.remark, scheduling.discovery, "#{buyer.name} [#{ShowBuyer.model_name.human}]", ExportTools.build_list(buyer.emails), ExportTools.build_list(buyer.phones), ExportTools.build_list(buyer.addresses), ExportTools.build_list(buyer.websites), buyer.network_list,buyer.custom_list, buyer.remark, ExportTools.build_list(buyer.people)
      ].to_csv

      expect(scheduling.to_csv).to eq(expected_line)
    end

    it "works with a show buyer and a period" do
      buyer = FactoryGirl.create(:show_buyer)
      scheduling = FactoryGirl.create(:scheduling, show_buyer: buyer, show_host: nil, period: nil)

      expected_line = [
        scheduling.full_name, scheduling.scheduler_name, scheduling.period, scheduling.prospecting_months, scheduling.contract_list, scheduling.style_list, scheduling.remark, scheduling.discovery, "#{buyer.name} [#{ShowBuyer.model_name.human}]", ExportTools.build_list(buyer.emails), ExportTools.build_list(buyer.phones), ExportTools.build_list(buyer.addresses), ExportTools.build_list(buyer.websites), buyer.network_list,buyer.custom_list, buyer.remark, ExportTools.build_list(buyer.people)
      ].to_csv

      expect(scheduling.to_csv).to eq(expected_line)
    end

    it "with a festival only" do
      festival = FactoryGirl.create(:festival, nb_edition: 2, last_year: '1999')
      scheduling = FactoryGirl.create(:scheduling, show_buyer: nil, show_host: festival)

      expected_line = [
        scheduling.full_name, scheduling.scheduler_name, I18n.t(scheduling.period, scope: 'simple_form.options.schedulings.period'), scheduling.prospecting_months, scheduling.contract_list, scheduling.style_list, 
        "Nb edition : 2 / Derniere annee : 1999", 
        scheduling.discovery, "#{festival.name} [#{Festival.model_name.human}]", ExportTools.build_list(festival.emails), ExportTools.build_list(festival.phones), 
        ExportTools.build_list(festival.addresses), ExportTools.build_list(festival.websites), festival.network_list,festival.custom_list, 
        festival.remark, ExportTools.build_list(festival.people)
      ].to_csv

      expect(scheduling.organizer).to respond_to(:nb_edition)
      expect(scheduling.to_csv).to eq(expected_line)
    end
  end

  describe "Organizer" do
    it "with a show buyer only" do
      buyer = FactoryGirl.create(:show_buyer)
      scheduling = FactoryGirl.create(:scheduling, show_buyer: buyer, show_host: nil)

      expect(scheduling.organizer).to eq(buyer)
      expect(scheduling.organizer_name_with_kind).to eq("#{buyer.name} [#{ShowBuyer.model_name.human}]")
    end

    it "with a show host only" do
      host = FactoryGirl.create(:venue)
      scheduling = FactoryGirl.create(:scheduling, show_buyer: nil, show_host: host)

      expect(scheduling.organizer).to eq(host)
      expect(scheduling.organizer_name_with_kind).to eq("#{host.name} [#{host.class.model_name.human}]")
    end

    it "with a show host and a show buyer" do
      buyer = FactoryGirl.create(:show_buyer)
      host = FactoryGirl.create(:venue)
      scheduling = FactoryGirl.create(:scheduling, show_buyer: buyer, show_host: host)

      expect(scheduling.organizer).to eq(buyer)
      expect(scheduling.organizer_name_with_kind).to eq("#{buyer.name} [#{ShowBuyer.model_name.human}]")
    end
  end

  describe "style_list for a ... " do
    it "festival" do
      festival = FactoryGirl.create(:festival)
      first_scheduling = FactoryGirl.create(:scheduling, show_host: festival, style_list: ['truc', 'muche'])
      second_scheduling = FactoryGirl.create(:scheduling, show_host: festival, style_list: ['bidule', 'truc'])
      expect(Scheduling.style_for(festival.reload).sort).to eq(['bidule', 'muche', 'truc'].sort)
    end

    it "venue" do
      venue = FactoryGirl.create(:venue)
      first_scheduling = FactoryGirl.create(:scheduling, show_host: venue, style_list: ['truc', 'muche'])
      second_scheduling = FactoryGirl.create(:scheduling, show_host: venue, style_list: ['bidule', 'truc'])
      expect(Scheduling.style_for(venue.reload).sort).to eq(['bidule', 'muche', 'truc'].sort)
    end

    it "show_buyer" do
      show_buyer = FactoryGirl.create(:show_buyer)
      first_scheduling = FactoryGirl.create(:scheduling, show_buyer: show_buyer, style_list: ['truc', 'muche'])
      second_scheduling = FactoryGirl.create(:scheduling, show_buyer: show_buyer, style_list: ['bidule', 'truc'])
      expect(Scheduling.style_for(show_buyer.reload).sort).to eq(['bidule', 'muche', 'truc'].sort)
    end
  end

  describe "#to_s" do
    it "works" do
      scheduling = FactoryGirl.build(:scheduling)
      expect(scheduling.to_s).to eq("#{scheduling.name} par #{scheduling.show_buyer.name}")
    end
  end

  describe "#prospecting_months_s" do
    it "works" do
      scheduling = FactoryGirl.build(:scheduling, prospecting_months: ["4", "12"])
      expect(scheduling.prospecting_months_s).to eq("avr.-déc.")
    end
  end

  describe "#export_filename" do
    it "works" do
      account = FactoryGirl.build(:account)
      expect(Scheduling.export_filename(account)).to eq("programmations-#{account.domain}.csv")
    end
  end

  describe "#show_host_kind" do
    it "works" do
      structure = FactoryGirl.create(:structure)
      scheduling = FactoryGirl.create(:scheduling, show_host: structure)
      expect(scheduling.show_host_kind).to eq(structure.class.name)
    end
  end

  describe "#show_host_name" do
    it "works" do
      show_host = FactoryGirl.create(:structure)
      scheduling = FactoryGirl.create(:scheduling, show_host: show_host)
      expect(scheduling.show_host_name).to eq(show_host.name)
    end
  end

  describe "#external_show_buyer" do
    it "works" do
      scheduling = FactoryGirl.create(:scheduling)
      scheduling.external_show_buyer = "Cette structure"
      expect(scheduling.show_buyer).to be_nil
    end
  end

  describe "#from_csv" do
    it "works" do
      csv = {}
      csv[:cycle_de_programmation] = ""
      csv[:contrats] = "cession, location, autre"
      csv[:decouverte] = "X"
      csv[:mois_prospection] = "8..12"
      csv[:nom_programmateur] = "Henri Ford"
      expect {
        scheduling = Scheduling.from_csv(csv)
        expect(scheduling).to be_kind_of(Scheduling)
      }.to_not raise_error
    end
  end

  describe "#csv_header" do
    it "works" do
      expected = "Nom programmation"
      expected << ",Programmateur"
      expected << ",Cycle de programmation"
      expected << ",Mois de prospection"
      expected << ",Types de contrat"
      expected << ",Styles"
      expected << ",Observations Programmation"
      expected << ",Scene découverte"
      expected << ",Nom,Emails,Téls,Adresses"
      expected << ",Sites web,Réseaux"
      expected << ",Tag Perso,Commentaires,Personnes"
      expected << "\n"
      expect(Scheduling.csv_header).to eq(expected)
    end
  end

  describe "#period_with_integer" do
    it "peut être par trimestre" do
      scheduling = FactoryGirl.create(:scheduling)
      scheduling.period_with_integer = 3
      expect(scheduling.period_with_integer).to eq("3")
    end

    it "peut être par semestre" do
      scheduling = FactoryGirl.create(:scheduling)
      scheduling.period_with_integer = 6
      expect(scheduling.period_with_integer).to eq("6")
    end

    it "peut être par année" do
      scheduling = FactoryGirl.create(:scheduling)
      scheduling.period_with_integer = 12
      expect(scheduling.period_with_integer).to eq("12")
    end

    it "ne peut pas être par mois" do
      scheduling = FactoryGirl.create(:scheduling)
      scheduling.period_with_integer = 1
      expect(scheduling.period_with_integer).to be_nil
    end
  end
end
