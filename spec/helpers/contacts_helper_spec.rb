require "rails_helper"

describe ContactsHelper, type: :helper do
  describe "#nav_link" do
    it "works" do
      stubs(:current_page?).returns("active")
      expect(nav_link("bla", "url", "truc")).to eq("<a href=\"url\" class=\"active\"><i class=\"entypo truc\"></i>bla</a>")
    end
  end

  describe "#tag_link" do
    it "works" do
      expect(tag_link("bla", "value", "Truc")).to eq("<a href=\"bla\" class=\"tag Truc\">value</a>")
    end
  end


end
