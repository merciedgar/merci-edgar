require "rails_helper"

describe ApplicationHelper, type: :helper do

  describe "stage_of" do
    context "room without stage info" do
      let(:room) { FactoryGirl.build(:room, depth: nil, width: nil, height: nil) }
      it { expect(stage_of(room)).to eq("Dimension plateau non précisée") }
    end

    context "room without stage info" do
      let(:room) { FactoryGirl.build(:room, depth: nil, width: 10, height: 1) }
      it { expect(stage_of(room)).to eq("Dimension plateau : P 0, L 10.0, H 1.0") }
    end

    context "room with stage info" do
      let(:room) { FactoryGirl.build(:room, depth: 10, width: 20, height: 2) }
      it { expect(stage_of(room)).to eq("Dimension plateau : P 10.0, L 20.0, H 2.0") }
    end
  end

  describe "name_of" do
    context "room without name" do
      let(:room) { FactoryGirl.build(:room, name: nil) }
      it { expect(name_of(room)).to eq("Salle") }
    end

    context "room with name" do
      let(:room) { FactoryGirl.build(:room, name: "Vivaldi") }
      it { expect(name_of(room)).to eq("Salle : Vivaldi") }
    end
  end

  describe "#display_base_errors" do
    it "empty without errors" do
      contact = FactoryGirl.build(:contact)
      expect(display_base_errors(contact)).to eq("")
    end

    it "show errors" do
      contact = FactoryGirl.build(:contact, name: nil)
      contact.errors[:base] = "truc"
      expect(display_base_errors(contact)).to eq("    <div class=\"alert alert-error alert-block\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&#215;</button>\n      <p>truc</p>\n    </div>\n")
    end
  end

  describe "#display_inline_errors" do
    it "works" do
      contact = FactoryGirl.build(:contact, name: nil)
      contact.save
      expect(display_inline_errors(contact)).to eq("<span class=\"help-inline\"><span class=\"noticetitle\">Oups ! Quelques erreurs se sont glissées !</span><span>Name doit être rempli(e) / </span></span>")
    end
  end

  describe "#get_errors" do
    it "works" do
      contact = FactoryGirl.build(:contact, name: nil)
      contact.save
      expect(get_errors(contact, false)).to eq("<span class=\"noticetitle\">Oups ! Quelques erreurs se sont glissées !</span><span>Name doit être rempli(e) / </span>")
    end
  end

  describe "#translate_multiple_values" do
    it "works" do
      expect(translate_multiple_values("bar, mjc", "simple_form.options.venue.kind")).to eq("Bar, MJC")
    end
  end
end
