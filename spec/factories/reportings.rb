# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :reporting do
    association :report, factory: :note_report
    association :asset, factory: :contact
  end
end

