FactoryGirl.define do
  factory :geonames_postal_code do
    country_code "FR"
    postal_code { Faker::Number.number(5) }
    place_name { Faker.name }
    latitude { Faker::Number.decimal(2) }
    longitude { Faker::Number.decimal(2) }
    accuracy 2
  end
end
