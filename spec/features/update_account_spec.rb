require 'rails_helper'

describe "Update account", :type => :feature do

  it "works" do
    account = FactoryGirl.create(:account)
    Account.current_id = account.id
    user = FactoryGirl.create(:user, accounts: [account])
    abilitation = account.abilitations.first
    abilitation.update_attribute(:kind, 'manager')

    login_as user
    Capybara.app_host = "http://#{account.domain}.lvh.me"

    visit edit_account_path(locale: :fr)

    expect(page).to have_content(user.name)
    expect(page).to have_content("Nom et domaine")
    expect(page).to have_selector("#edit_account_#{account.id}", visible: true)

    within("#edit_account_#{account.id}") do
      fill_in 'account_name', with: 'un nouveau nom'
      click_button 'Enregistrer les modifications'
    end

    account.reload
    expect(account.name).to eq('un nouveau nom')


  end
end


