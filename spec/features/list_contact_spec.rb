require 'rails_helper'

describe "list contact", :type => :feature do

  it "show only contact from same account" do
    other_contact = FactoryGirl.create(:venue, account: FactoryGirl.create(:account))

    account = FactoryGirl.create(:account)
    Account.current_id = account.id

    user = FactoryGirl.create(:admin, accounts: [account])
    login_as user
    Capybara.app_host = "http://#{account.domain}.lvh.me"

    my_contact = FactoryGirl.create(:venue, account: account)

    visit contacts_path
    expect(page).to have_content 'Contacts'
    expect(page).to have_content my_contact.name
    expect(page).to_not have_content other_contact.name
  end
end
