require 'rails_helper'

describe "Create a venue", :type => :feature do

  it "simple" do
    account = FactoryGirl.create(:account)
    user = FactoryGirl.create(:admin, accounts: [account])
    login_as user
    Capybara.app_host = "http://#{account.domain}.lvh.me"

    visit new_structure_path
    expect(page).to have_content 'Contacts'
    expect(page).to have_content 'Lieu'
  end
end

