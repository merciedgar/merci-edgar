require 'rails_helper'
require 'sidekiq/testing'

Sidekiq::Testing.inline!

describe SendUpgradeReceiptEmailWorker do
  describe "perform" do
    it "respond to perform_async" do
      expect(SendUpgradeReceiptEmailWorker).to respond_to(:perform_async)
    end

    it "works" do
      account = FactoryGirl.create(:account)
      manager = FactoryGirl.create(:user)
      s = SendUpgradeReceiptEmailWorker.new
      s.perform(account.id, manager.id, 100)
    end
  end
end
