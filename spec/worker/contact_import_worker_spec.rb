require "rails_helper"

describe ContactsImportWorker do

  it 'respond to #perform_async' do
    expect(ContactsImportWorker).to respond_to(:perform_async)
  end

  it "can perform" do
    contactsImport = FactoryGirl.create(:contacts_import)
    ContactsImportWorker.any_instance.stubs(:import_contacts_with) { true }
    expect {
      ContactsImportWorker.new.perform(contactsImport.id.to_s)
    }.to_not raise_error
  end

  it "works" do
    import = FactoryGirl.create(:contacts_import)
    stored_file = Object.new
    stored_file.stubs(:cache_stored_file!).returns("fakefile")
    stored_file.stubs(:filename).returns("filename")
    stored_file.stubs(:file).returns("a file")
    ContactsImportWorker.any_instance.stubs(:import_spreadsheet_file)
    ContactsImportWorker.any_instance.stubs(:payload=)
    mailer = Object.new
    mailer.stubs(:deliver).returns(true)
    UserMailer.stubs(:contacts_import_email).returns(mailer)
    import.stubs(:contacts_file).returns(stored_file)
    import.stubs(:filename).returns("A filename from import")

    ContactsImportWorker.new.import_contacts_with(import)

    expect(Account.first.importing_now).to be_nil
  end

end

