class ConfirmationsController < Devise::PasswordsController
  skip_before_filter :require_no_authentication
  skip_before_filter :authenticate_user!

  def create
    self.resource = resource_class.send_confirmation_instructions(resource_params)
    if successfully_sent?(resource)
      respond_with({}, :location => after_resending_confirmation_instructions_path_for(resource_name))
    else
      respond_with(resource)
    end
  end

  def update
    with_unconfirmed_confirmable do
      @confirmable.attempt_set_password(params[:user])
      if @confirmable.valid?
        do_confirm
      else
        do_show
        @confirmable.errors.clear #so that we won't render :new
      end
    end

    if !@confirmable.errors.empty?
      render 'devise/confirmations/new'
    end
  end

  def show
    with_unconfirmed_confirmable do
      if @confirmable.has_no_password?
        do_show
      else
        do_confirm
      end
    end
    if !@confirmable.errors.empty?
      render 'devise/confirmations/new'
    end
  end

  protected

  def with_unconfirmed_confirmable
    @confirmable = User.find_or_initialize_with_error_by(:confirmation_token, params[:confirmation_token])
    self.resource = @confirmable
    if !@confirmable.new_record?
      @confirmable.only_if_unconfirmed {yield}
    end
  end

  def do_show
    @confirmation_token = params[:confirmation_token]
    @confirmable.label_name = params[:label_name]
    @requires_password = true
    # @confirmable.accounts.destroy_all
    render 'devise/confirmations/show'
  end

  def do_confirm
    @confirmable.confirm!
    abilitation = @confirmable.abilitations.first
    unless @confirmable.abilitations.any?
      abilitation = @confirmable.abilitations.build
      abilitation.build_account(name: @confirmable.label_name)
      abilitation.kind = "manager"
    end
    @confirmable.save!
    set_flash_message :notice, :confirmed
    logger.debug "before signin and redirect"
    sign_in(@confirmable)
    redirect_to "#{request.protocol}#{abilitation.account.domain}.#{request.domain}:#{request.port}#{new_user_session_path}"        
  end

  def after_resending_confirmation_instructions_path_for(resource_name)
    new_session_path(resource_name)
  end

  def after_confirmation_path_for(resource_name, resource)
    after_sign_in_path_for(resource)
  end
end
